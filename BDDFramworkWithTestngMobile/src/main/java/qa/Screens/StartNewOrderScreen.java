package qa.Screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import bddBase.QA.TestBase;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class StartNewOrderScreen extends TestBase {


	@FindBy(xpath = "//a[@class='btn wb-btn-default start-btn'][contains(text(),'Start New Order')]")
	public WebElement StartNewOrderbutton;

	@FindBy(how = How.XPATH, using = " //div[@class='col-lg-6 col-md-6 col-sm-12 col-xs-12']//div[@class='row']//span[contains(text(),'1 Item')]")
	@CacheLookup
	public WebElement taponfavoriteorder;

	public StartNewOrderScreen() {

		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		
	}

	// Tap on Start New Order button
	public void TaponStartNewOrderbutton() throws Exception {

		// // wait.until(ExpectedConditions.visibilityOf(StartNewOrderbutton));
		// Actions actions = new Actions(driver);
		// actions.moveToElement(driver.findElement(By.xpath("//a[contains(text(),'Start
		// New Order')]")));
		// actions.click();
		// actions.build().perform();
		// taponfavoriteorder.click();

		StartNewOrderbutton.click();

	}

}
