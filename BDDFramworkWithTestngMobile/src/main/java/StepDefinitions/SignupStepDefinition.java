package StepDefinitions;

import bddBase.QA.TestBase;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import qa.Screens.SignUpWithScreen;
import qa.Screens.WelcomeScreen;

public class SignupStepDefinition extends TestBase {
	WelcomeScreen welcomescreen;
	SignUpWithScreen signupwithscreen;
	
	
	
	@Given("^User is in sign up page$")
	public void user_is_in_sign_up_page()  {
		Appiumstart();
		try {
			welcomescreen.Taponhamaburgermenu();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		welcomescreen.TaponSignupbutton();
	    
	}

	@When("^user enters details in all fields$")
	public void user_enters_details_in_all_fields(DataTable arg1)  {
	    
	}

	@Then("^user enters email id$")
	public void user_enters_email_id()  {
	    
	}

	@Then("^user taps on email checkbox & tap on Signup button$")
	public void user_taps_on_email_checkbox_tap_on_Signup_button()  {
		
	    
	}

	@Then("^user signout$")
	public void user_signout()  {
	    
	}

}
