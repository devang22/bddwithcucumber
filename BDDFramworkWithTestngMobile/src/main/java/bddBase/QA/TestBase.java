package bddBase.QA;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;


public class TestBase {

	public static Properties prop;
	public static EventFiringWebDriver e_driver;
	// public static CrmEventListener eventListener;
	public static AndroidDriver<MobileElement> driver;

	public TestBase() {
		// To read and fetch data from config file
		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream(
					"D:\\workspace\\MobileBrowserTestTDDFramwork\\src\\main\\java\\crm\\qa\\config\\config.properties");
			prop.load(ip);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();

		}
	}

	public static AndroidDriver<MobileElement> Appiumstart()  {

		// Instantiate Appium Driver

		try {

			// Set the Desired Capabilities
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability("deviceName", "803KPFX1714131");
			// caps.setCapability("udid", "ENUL6303030010"); //Give Device ID of your mobile
			// phone
			caps.setCapability("platformName", "Android");
			caps.setCapability("platformVersion", "9");
			caps.setCapability("browserName", "Chrome");
			caps.setCapability("noReset", true);
			caps.setCapability("chromedriverExecutable",
					"D:\\workspace\\WhataburgerCrossBrowserTest\\exe\\chromedriver.exe");
			// caps.setCapability("automationName", "appium");
			// Set ChromeDriver location
			System.setProperty("webdriver.chrome.driver",
					"D:\\workspace\\WhataburgerCrossBrowserTest\\exe\\chromedriver.exe");
			driver = new AndroidDriver<MobileElement>(new URL("http://0.0.0.0:4723/wd/hub"), caps);
			driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);

		} catch (MalformedURLException e) {
			System.out.println(e.getMessage());
		}

		// Open URL in Chrome Browser
		driver.get(prop.getProperty("url"));

		return driver;

	}
}
