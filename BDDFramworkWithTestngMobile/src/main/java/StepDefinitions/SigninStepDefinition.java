package StepDefinitions;

import bddBase.QA.TestBase;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import qa.Screens.HomeScreen;
import qa.Screens.SignInWithScreen;
import qa.Screens.WelcomeScreen;

public class SigninStepDefinition extends TestBase{
    WelcomeScreen welcomescreen;
    HomeScreen homescreen;
    SignInWithScreen signinwithscreen;
	
	@Given("^user is in welcome page$")
	public void user_is_in_welcome_page() throws Exception {
		Appiumstart();
		welcomescreen= new WelcomeScreen();
		welcomescreen.Taponhamaburgermenu();
		welcomescreen.TaponSigninbutton();
		
	}

	@When("^user enters \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_enters_and(String username, String password) throws Exception{
		signinwithscreen  = new SignInWithScreen();
		signinwithscreen.Signin(username, password);
	    
	}

	@Then("^user is on Home Page and verify wab logo$")
	public void user_is_on_Home_Page_and_verify_wab_logo() {
		homescreen = new  HomeScreen();
		
	    
	}

	@Then("^user signout the app$")
	public void user_signout_the_app()  {
		try {
			homescreen.TaponSignoutbutton();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    try {
			homescreen.TaponSignoutpopupbutton();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Then("^Close the browser$")
	public void close_the_browser() throws Throwable {
		driver.quit();
	   
	}

}
