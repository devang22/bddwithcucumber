package qa.Screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import bddBase.QA.TestBase;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class WelcomeScreen extends TestBase {
	WebDriverWait wait;
	

	@FindBy(id = "navbar-icon-bar")
	public WebElement hamburgermenu;

	@FindBy(xpath = "//span[contains(text(),'Sign in')]")
	public WebElement signinbtn;

	@FindBy(xpath = "//a[contains(text(),'Sign Out')]")
	public WebElement Signoutbutton;

	@FindBy(xpath = "//a[contains(text(),'Yes, Sign Out')]")
	public WebElement YesSignoutbutton;
	

	public WelcomeScreen() {

		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		wait = new WebDriverWait(driver, 90);
	}

	// Tap on Hamburger menu
	public void Taponhamaburgermenu() throws Exception {
		// wait.until(ExpectedConditions.visibilityOf(allowbutton));
		// allowbutton.isDisplayed();
		// allowbutton.click();
		// wait.until(ExpectedConditions.visibilityOf(hamburgermenu));

		hamburgermenu.click();
	}

	// Tap on Signin button
	public void TaponSigninbutton() throws Exception {
		signinbtn.click();
		Thread.sleep(8000);

	}

	public void Signout() {
		Signoutbutton.click();
		YesSignoutbutton.click();
	}
	
	public void TaponSignupbutton() {
		signinbtn.click();
		
	}
}
