Feature: Whataburger Login Feature 

#with examples keyword with datadriven 

@SmokeTest 
Scenario Outline: Whataburger Login Test Scenario 
 
	Given user is in welcome page 
	When user enters "<username>" and "<password>" 
	Then user clicks on SignIn button 
	Then user is on Home Page 
	Then close browser 
	
	
	Examples: 
		| username 			  | password |
		|devangwa998@gmail.com| burger01 | 
		|wabburger@gmail.com  | burger01 |
		
#		@First 
#		@RegressionTest 
#		Scenario Outline: Whataburger Login with invalid credentals Sceanrio 
#		
#			Given user is on welcome page 
#			Then user navigate to sign up page 
#			Then user enter "<username>" and "<password>" 
#			Then user taps on signin button 
#			Then user signout web 
#			Then close browser 
#			
#			
#			Examples: 
#				|asj@gmail.com|qwerty01@|
				
 