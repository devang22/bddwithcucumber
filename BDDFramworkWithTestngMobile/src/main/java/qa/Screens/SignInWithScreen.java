package qa.Screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import bddBase.QA.TestBase;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SignInWithScreen extends TestBase {
	WebDriverWait wait;

	@FindBy(id = "username")
	public WebElement Emailid;

	@FindBy(id = "password")
	public WebElement Password;

	@FindBy(xpath = "//button[contains(text(),'Sign In')]")
	public WebElement Signinbtn;

	@FindBy(xpath = "//a[contains(text(), 'Sign Out')]")
	public WebElement Signoutbtn;

	@FindBy(xpath = "//button[contains(text(), 'Yes, Sign Out')]")
	public WebElement YesSignoutbtn;

	@FindBy(xpath = "//span[contains(text(), 'View My Account')]")
	public WebElement ViewMyAccountbtn;

	public SignInWithScreen() {

		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		// wait = new WebDriverWait(driver, 90);
	}

	// Enter Email id, Password, Tap on Sign In button
	public void Signin(String username, String password) throws Exception {
		System.out.println("enter credentials");
		Emailid.sendKeys(username);
		Password.sendKeys(password);
		Signinbtn.click();
	}

	// Tap on Sign Out button in Home screen and tap on Yes Sign Out button in pop
	// up
	public HomeScreen Signout() throws Exception {

		Thread.sleep(5000);
		Signoutbtn.click();
		YesSignoutbtn.click();
		return new HomeScreen();

	}

	// Tap on View My Account button
	public void TaponViewMyAccountbutton() throws Exception {
		Thread.sleep(5000);
		ViewMyAccountbtn.click();

	}
}
